<?php
header('Content-Type: application/json');
include 'Enregistrements.php';
$probleme = $_POST['probleme'];
$priorite = $_POST['priorite'];
$date = $_POST['date'];
$latitude = $_POST['lat'];
$longitude = $_POST['lon'];

// echo "Le probleme est ".$probleme."<br>";
// echo "La priorite est ".$priorite."<br>";
// echo "La date est ".$date."<br>";
// echo "La latitude est ".$latitude."<br>";
// echo "La longitude est ".$longitude."<br>";
$arrayPoint = array();
try
{
    $bool = true; 
    $message = "";

    if (!isset($probleme) || empty($probleme) ) {
        $bool = false;
        $message = $message."Le probleme est manquant<br>";
    }
    if (!isset($priorite) || empty($priorite)) {
        $bool = false;
        $message = $message."La priorité est manquante<br>";
    }
    if (!isset($date) || empty($date)) {
        $bool = false;
        $message = $message."La date est manquante<br>";
    }
    if (!isset($latitude) || empty($latitude)) {
        $bool = false;
        $message = $message."La latitude est manquante<br>";
    }
    if (!isset($longitude) || empty($longitude)) {
        $bool = false;
        $message = $message."La longitude est manquante<br>";
    }

    if ($bool) {
     $enregistrements = new Enregistrements();
     $enregistrements->putIn($probleme, $priorite, $date, $latitude, $longitude);

     $enregistrements = $enregistrements->getAll();
        // var_dump($enregistrements);

     foreach ($enregistrements as $en) {
        $point['type'] = 'point';
        $point['longitude'] = $en['longitude'];
        $point['latitude'] = $en['latitude'];
        $arrayPoint[] = $point;
    }
}else{

    $reponse['erreur'] = '404';
    $reponse['content'] = $message;

}


}catch(Exception $e)
{

   $reponse['ok'] = '404';
   $reponse['content'] = $e->getMessage();
   echo json_encode($reponse);

}

// On ajoute une entrée dans la table jeux_video

?>