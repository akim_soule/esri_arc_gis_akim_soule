<?php

class Modele {

	private $bdd;
	//Methode qui execute les requetes
	protected function executerRequete($sql, $params = null) {
		if ($params == null) {
			$resultat = $this->getBdd()->query($sql);// exécution directe
		} else {
			$resultat = $this->getBdd()->prepare($sql);// requête préparée
			$resultat->execute($params);
		}
		return $resultat;
	}
	//Methode pour acceder a la BD
	private function getBdd() {
		if ($this->bdd == null) {
			// Création de la connexion
			$this->bdd = new PDO('mysql:host=localhost;dbname=Carte;charset=utf8',
				'root', 'root',
				array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
		}
		return $this->bdd;
	}
}

?>