<?php
header('Content-Type: application/json');
include 'Enregistrements.php';
$probleme = $_POST['probleme'];
$priorite = $_POST['priorite'];
$date = $_POST['date'];
$latitude = $_POST['latitude'];
$longitude = $_POST['longitude'];

// echo "Le probleme est ".$probleme."<br>";
// echo "La priorite est ".$priorite."<br>";
// echo "La date est ".$date."<br>";
// echo "La latitude est ".$latitude."<br>";
// echo "La longitude est ".$longitude."<br>";
$arrayPoint = array();
try
{
    $bool = true; 
    $message = "";

    if (!isset($probleme) || empty($probleme) ) {
        $bool = false;
        $message = $message."Le probleme est manquant\n";
    }
    if (!isset($priorite) || empty($priorite)) {
        $bool = false;
        $message = $message."La priorité est manquante\n";
    }
    if (!isset($date) || empty($date)) {
        $bool = false;
        $message = $message."La date est manquante\n";
    }
    if (!isset($latitude) || empty($latitude)) {
        $bool = false;
        $message = $message."La latitude est manquante\n";
    }
    if (!isset($longitude) || empty($longitude)) {
        $bool = false;
        $message = $message."La longitude est manquante\n";
    }

    if ($bool) {
     $enregistrements = new Enregistrements();
     $enregistrements->putIn($probleme, $priorite, $date, $latitude, $longitude);

     $enregistrements = $enregistrements->getAll();
        // var_dump($enregistrements);

     foreach ($enregistrements as $en) {
        $point['type'] = 'point';
        $point['longitude'] = $en['longitude'];
        $point['latitude'] = $en['latitude'];
        $arrayPoint[] = $point;
    }
    $reponse['ok'] = '202';
    $reponse['content'] = $arrayPoint;
}else{

    $reponse['erreur'] = '404';
    $reponse['content'] = $message;

}


}catch(Exception $e)
{

   $reponse['erreur'] = '404';
   $reponse['content'] = $e->getMessage();

}
echo json_encode($reponse);
// On ajoute une entrée dans la table jeux_video

?>