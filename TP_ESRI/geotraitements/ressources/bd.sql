drop database  if exists Carte;
create database Carte;
use Carte;
drop  table if exists Enregistrements;
create table Enregistrements (
	id int  primary key auto_increment,
    probleme varchar(255),
    priorite int,
    date_ date,
    latitude float(8,3),
    longitude float(8,3)
);

select * from enregistrements;
